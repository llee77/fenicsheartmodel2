from dolfin import *

class NeoHookean(object):

    def __init__(self, Fp = None, params = None):

	self.Fp = Fp

        if params is None:
            params = self.default_parameters()

        for k,v in params.iteritems():
            setattr(self, k, v)



    def default_parameters(self):
        return {"mu":10000}



    def strain_energy(self, F):


	J = det(F)
	if (self.Fp is None):
		Fe = F
	else:
		Fe = F*(self.Fp)

	Fe_dev = J**(-1.0/3.0)*Fe
	Ce_dev = Fe_dev.T*Fe_dev
	Ic = tr(Ce_dev)

	mu = self.mu

	Wtotal = (mu/2.0)*(Ic - 3.0)

	return Wtotal
		

class FungTransverse(object):

    def __init__(self, Fg = None, f0 = None, s0 = None, n0 = None, params = None):

	self.Fg = Fg
	self.f0 = f0
	self.s0 = s0
	self.n0 = n0

        print params

        if params is None:
            params = self.default_parameters()

        for k,v in params.iteritems():
            setattr(self, k, v)



    def default_parameters(self):
        return {"C" : 191,
                "bf": 92.6,
		"bfx": 49.27,
                "bxx": 1.11}



    def strain_energy(self, F):


	if (self.Fg is None):
		Fe = F
	else:
		Fe = F*inv(self.Fg)

	J = det(Fe)

	Fe_dev = J**(-1.0/3.0)*Fe
	Ce_dev = Fe_dev.T*Fe_dev
	Edev = Constant(0.5)*(Ce_dev - Identity(3))

	Eff = inner(self.f0, Edev*self.f0)
	Ess = inner(self.s0, Edev*self.s0)
	Enn = inner(self.n0, Edev*self.n0)
	Efs = inner(self.f0, Edev*self.s0)
	Efn = inner(self.f0, Edev*self.n0)
	Ens = inner(self.n0, Edev*self.s0)

	C = self.C
	bf = self.bf
	bfx = self.bfx
	bxx = self.bxx

	QQ = bf*pow(Eff,2.0) + bxx*(pow(Ess,2.0)+ pow(Enn,2.0)+ 2.0*pow(Ens,2.0)) + bfx*(2.0*pow(Efs,2.0) + 2.0*pow(Efn,2.0))

	WFung = C*(exp(QQ) - 1.0)

	return WFung
		

