#### IN PROGRESS #########################################################
from dolfin import *

class SimpleActive(object):

    def __init__(self, dt = None, lscprev = None, t_a = None, Tact = None, f0 = None, s0 = None, n0 = None, params = None):

	self.f0 = f0
	self.s0 = s0
	self.n0 = n0
	self.Tact = Tact
	self.t_a = t_a
	self.lscprev = lscprev
	self.dt = dt

        print params

        if params is None:
            params = self.default_parameters()

        for k,v in params.iteritems():
            setattr(self, k, v)

    def default_parameters(self):
        return {"a6" : 2.0,
		"a7" : 1.5,
		"Ea" : 20.0,
		"v0" : 7.5e-3,
		"ls0" : 1.85,
		"tr" : 100,#75,
		"td" : 300,#75,
		"b" : 0.20e3,#0.15,
		"ld" : -0.4,
        	"T0" : 180e3,
		};

    def strain_energy(self, F):

	Cmat = F.T*F

	I4f = inner(self.f0, Cmat*self.f0)

	Tref = self.Tref

	Wactive = self.Tact * (I4f - 1.0)

	return Wactive

    def weakform(self, F, v, lsc, lsct):

	a6 = self.a6
	a7 = self.a7
	Ea = self.Ea
	v0 = self.v0
	ls0 = self.ls0
	tr = self.tr
	td = self.td
	b = self.b
	ld = self.ld
	T0 = self.T0
	print td
	print b
	print ls0
	
	Cmat = F.T*F
	lmbda = sqrt(dot(self.f0, Cmat*self.f0))
	ls = lmbda*ls0
	
	xp1 = conditional(gt(lsc,a7), 1.0, 0.0)
	fiso = (T0*tanh(a6*(lsc - a7))**2.0)*xp1
	
	tmax = b*(ls - ld)
	xp2 = conditional(lt(self.t_a,tmax), 1.0, 0.0)
	ftwitch = (tanh(self.t_a/tr)**2.0*tanh((tmax - self.t_a)/td)**2.0)*xp2

	M1ij = self.f0[i]*self.f0[j]

	Pact = lmbda*Ea*fiso*ftwitch*(ls - lsc)

	Pact_tensor = F*Pact*as_tensor(M1ij, (i,j)) 

	lscnew = lsc - self.lscprev - self.dt*(Ea*(ls - lsc) - 1.0)*v0

	return inner(Pact_tensor, grad(v)) + inner(lsct, lscnew)


	
	
		

	

