from dolfin import *
from compressibility import get_compressibility
from copy import deepcopy
import math

log_level = INFO

# Setup logger
def make_logger(name, level = INFO):
    import logging

    mpi_filt = lambda: None
    def log_if_proc0(record):
        if dolfin.mpi_comm_world().rank == 0:
            return 1
        else:
            return 0
    mpi_filt.filter = log_if_proc0

    logger = logging.getLogger(name)
    logger.setLevel(log_level)

    ch = logging.StreamHandler()
    ch.setLevel(level)

    formatter = logging.Formatter('%(message)s') #'\n%(name)s - %(levelname)s - %(message)s\n'
    ch.setFormatter(formatter)

    logger.addHandler(ch)
    logger.addFilter(mpi_filt)

    dolfin.set_log_active(True)
    dolfin.set_log_level(dolfin.WARNING)

    return logger

logger = make_logger("Adjoint_Contraction", log_level)

class Solver(object):

    """
    A Generic Solver
    """
    
    def __init__(self, params):        

        for k in ["mesh", "facet_function", "material", "bc"]:
            assert params.has_key(k), \
              "{} need to be in solver_parameters".format(k)

        
        self.parameters = params

        # Krylov solvers does not work
        self.iterative_solver = False

        # Update solver parameters
        if params.has_key("solve"):
            if params["solve"].has_key("nonlinear_solver") \
              and params["solve"]["nonlinear_solver"] == "newton":
                self.use_snes == False
            else:
                self.use_snes = True
              
            prm = self.default_solver_parameters()

            for k, v in params["solve"].iteritems():
                if isinstance(params["solve"][k], dict):
                    for k_sub, v_sub in params["solve"][k].iteritems():
                        prm[k][k_sub]= v_sub

                else:
                    prm[k] = v
        else:
            prm = self.default_solver_parameters()
            
        self.parameters["solve"] = prm
        
        self._init_spaces()
        self._init_forms()

    def default_solver_parameters(self):

        nsolver = "snes_solver" if self.use_snes else "newton_solver"

        prm = {"nonlinear_solver": "snes", "snes_solver":{}} if self.use_snes else {"nonlinear_solver": "newton", "newton_solver":{}}

        prm[nsolver]['absolute_tolerance'] = 1E-5
        prm[nsolver]['relative_tolerance'] = 1E-5
        prm[nsolver]['maximum_iterations'] = 8
        # prm[nsolver]['relaxation_parameter'] = 1.0
        prm[nsolver]['linear_solver'] = 'lu'
        prm[nsolver]['error_on_nonconvergence'] = True
        prm[nsolver]['report'] = True if logger.level < INFO else False
        if self.iterative_solver:
            prm[nsolver]['linear_solver'] = 'gmres'
            prm[nsolver]['preconditioner'] = 'ilu'

            prm[nsolver]['krylov_solver'] = {}
            prm[nsolver]['krylov_solver']['absolute_tolerance'] = 1E-9
            prm[nsolver]['krylov_solver']['relative_tolerance'] = 1E-7
            prm[nsolver]['krylov_solver']['maximum_iterations'] = 1000
            prm[nsolver]['krylov_solver']['monitor_convergence'] = False
            prm[nsolver]['krylov_solver']['nonzero_initial_guess'] = False

            prm[nsolver]['krylov_solver']['gmres'] = {}
            prm[nsolver]['krylov_solver']['gmres']['restart'] = 40

            prm[nsolver]['krylov_solver']['preconditioner'] = {}
            prm[nsolver]['krylov_solver']['preconditioner']['structure'] = 'same_nonzero_pattern'

            prm[nsolver]['krylov_solver']['preconditioner']['ilu'] = {}
            prm[nsolver]['krylov_solver']['preconditioner']['ilu']['fill_level'] = 0

        return prm
           
        
    def get_displacement(self, name = "displacement", annotate = True):
        return self._compressibility.get_displacement(name, annotate)

    def get_u(self):
        if self._W.sub(0).num_sub_spaces() == 0:
            return self._w
        else:
            return split(self._w)[0]

    def get_gamma(self):
        return self.parameters["material"].gamma

    def is_incompressible(self):
        return self._compressibility.is_incompressible()

    def get_state(self):
	if(self._w == "displacement-pressure"):
		print "HERE"
        return self._w

    def get_state_space(self):
        return self._W
    
    def reinit(self, w):
        """
        *Arguments*
          w (:py:class:`dolfin.GenericFunction`)
            The state you want to assign

        Assign given state, and reinitialize variaional form.
        """
        self.get_state().assign(w, annotate=False)
        self._init_forms()
    

    def solve(self):
        r"""
        Solve the variational problem

        .. math::

           \delta W = 0

        """
        # Get old state in case of non-convergence
        w_old = self.get_state().copy(True)
        try:
            # Try to solve the system
             solve(self._G == 0,
                   self._w,
                   self._bcs,
                   J = self._dG,
                   solver_parameters = self.parameters["solve"])
                   #annotate = False)

        except RuntimeError:
            # Solver did not converge
            logger.warning("Solver did not converge")
            # Retrun the old state, and a flag crash = True
            self.reinit(w_old)
            return w_old, True

#        else:
#            # The solver converged
#            
#            # If we are annotating we need to annotate the solve as well
#            if not parameters["adjoint"]["stop_annotating"]:
#
#                # Assign the old state
#                self._w.assign(w_old)
#                # Solve the system with annotation
#                # FIXME: This should be done more effiient than calling
#                # solve two times
#                solve(self._G == 0,
#                      self._w,
#                      self._bcs,
#                      J = self._dG)
#                      #solver_parameters = self.parameters["solve"], 
#                      #annotate = True)
#
#            # Return the new state, crash = False
#            return self._w, False

    def internal_energy(self):
        """
        Return the total internal energy
        """
        return self._pi_int

    def first_piola_stress(self):
        r"""
        First Piola Stress Tensor

        Incompressible:

        .. math::

           \mathbf{P} =  \frac{\partial \psi}{\partial \mathbf{F}} - p\mathbf{F}^T

        Compressible:

        .. math::

           \mathbf{P} = \frac{\partial \psi}{\partial \mathbf{F}}
        
        """

        if self.is_incompressible():
            p = self._compressibility.p
            return diff(self._strain_energy, self._F) - p*self._F.T
        else:
            return diff(self._strain_energy, self._F)

    def second_piola_stress(self):
        r"""
        Second Piola Stress Tensor

        .. math::

           \mathbf{S} =  \mathbf{F}^{-1} \mathbf{P}

        """
        return inv(self._F)*self.first_piola_stress()

    def chaucy_stress(self):
        r"""
        Chaucy Stress Tensor

        Incompressible:

        .. math::

           \sigma = \mathbf{F} \frac{\partial \psi}{\partial \mathbf{F}} - pI

        Compressible:

        .. math::

           \sigma = \mathbf{F} \frac{\partial \psi}{\partial \mathbf{F}}
        
        """
        if self.is_incompressible():
            p = self._compressibility.p
            return self._F*diff(self._strain_energy, self._F) - p*self._I 
        else:
            J = det(self._F)
            return J**(-1)*self._F*diff(self._strain_energy, self._F)

    def fiber_stress(self):
        r"""Compute Fiber stress

        .. math::

           \sigma_{f} = f \cdot \sigma f,

        with :math:`\sigma` being the Chauchy stress tensor
        and :math:`f` the fiber field on the current configuration

        """
        # Fibers
        f0 = self.parameters["material"].f0
        f = self._F*f0

        return inner(f, self.chaucy_stress()*f)

    def fiber_strain(self):
        r"""Compute Fiber strain

        .. math::

           \mathbf{E}_{f} = f \cdot \mathbf{E} f,

        with :math:`\mathbf{E}` being the Green-Lagrange strain tensor
        and :math:`f` the fiber field on the current configuration

        """
        # Fibers
        f0 = self.parameters["material"].f0
        f = self._F*f0

        return inner(f, self._E*f)

    def fiber_stretch(self):
        r"""Compute Fiber stretch

        .. math::

           \mathbf{E}_{f} = f \cdot \mathbf{C} f,

        with :math:`\mathbf{C}` being the Green-Lagrange stretch tensor
        and :math:`f` the fiber field on the current configuration

        """
        # Fibers
        f0 = self.parameters["material"].f0

        return inner(f0, self._C*f0)



    def work(self):
        r"""
        Compute Work

        .. math::

           W = \mathbf{S} : \mathbf{E},

        with :math:`\mathbf{E}` being the Green-Lagrange strain tensor
        and :math:`\mathbf{E}` the second Piola stress tensor
        """
        return inner(self._E, self.second_piola_stress())

    def cavityvol(self):

	
	N = self.parameters["facet_normal"]
	X = SpatialCoordinate(self.parameters["mesh"])
	if(self.parameters["volume_control"]):
		# Hack to include one more function space for active contraction
        	u,p,Pendo,lsc = self.get_state().split()
	else:
        	u,p = self.get_state().split()
        ds = Measure("exterior_facet", subdomain_data \
                     = self.parameters["facet_function"])

        endoid = self.parameters["endoid"]

    	vol_form = -Constant(1.0/3.0) * inner(det(self._F)*dot(inv(self._F).T, N), X + u)*ds(endoid)

	return assemble(vol_form)


    def cavitypressure(self):

	dofmap = self._compressibility.get_cavpressure_space().dofmap()
    	val_dof = dofmap.cell_dofs(0)[0]
    	pressure = self.get_state().vector()[val_dof][0]

    	return pressure
	

        

    def _init_spaces(self):
        """
        Initialize function spaces
        """
        
        self._compressibility = get_compressibility(self.parameters)
            
        self._W = self._compressibility.W
        self._w = self._compressibility.w
        self._w_test = self._compressibility.w_test


    def _init_forms(self):
        r"""
        Initialize variational form

        """
        material = self.parameters["material"]
	activematerial = self.parameters["activematerial"]
        N =  self.parameters["facet_normal"]
        ds = Measure("exterior_facet", subdomain_data \
                     = self.parameters["facet_function"])
	V0 = self.parameters["constrained_vol"]


        # Displacement
        u = self._compressibility.get_displacement_variable()
	lsc = self._compressibility.get_state_variable()

	if(self.parameters["volume_control"]):
		Pendo = self._compressibility.get_cavpressure_variable()
        self._I = Identity(self.parameters["mesh"].topology().dim())
	self._X = SpatialCoordinate(self.parameters["mesh"])
	x = u + self._X

        # Deformation gradient
        F = grad(u) + self._I
        self._F = variable(F)
        J = det(self._F)
        self._C = F.T*F
        self._E = 0.5*(self._C - self._I)
        n = cofac(F)*N
        f0 = self.parameters["material"].f0

        # Internal energy
        self._strain_energy = material.strain_energy(F) #+ activematerial.strain_energy(F)
        self._pi_int = self._strain_energy*dx + self._compressibility(J)*dx   
	
        endoid = self.parameters["endoid"]

	if self.parameters["volume_control"]:
		dsendo = ds(endoid, domain = self.parameters["mesh"], subdomain_data = self.parameters["facet_function"])
		area = assemble(Constant(1.0) * dsendo)
        	V_u = - Constant(1.0/3.0) * inner(x, n)
		self._pi_int += (Constant(1.0/area) * Pendo  * V0 * dsendo) - (Pendo * V_u *dsendo)

	# Internal virtual work
        self._G = derivative(self._pi_int, self._w, self._w_test) 
        
        # External work
        v = self._compressibility.u_test
	lsct = self._compressibility.s1_test

	# Add active material
	self._G += activematerial.weakform(F, v, lsc, lsct)*dx

        # Neumann BC
        if self.parameters["bc"].has_key("neumann"):
            for neumann_bc in self.parameters["bc"]["neumann"]:
                p, marker = neumann_bc
                self._G += inner(J*p*inv(F).T*N, v)*ds(marker)

        
        # Robin BC
        if self.parameters["bc"].has_key("robin"):
            for robin_bc in self.parameters["bc"]["robin"]:
                val, marker = robin_bc
                self._G += -inner(val*u, v)*ds(marker)
        
        # Other body forces
        if self.parameters.has_key("body_force"):
            self._G += -inner(self.parameters["body_force"], v)*dx

        # Dirichlet BC
        if self.parameters["bc"].has_key("dirichlet"):
            if hasattr(self.parameters["bc"]["dirichlet"], '__call__'):
                self._bcs = self.parameters["bc"]["dirichlet"](self._W)
            else:
                self._bcs = self._make_dirichlet_bcs()

        
        self._dG = derivative(self._G, self._w)

    def _make_dirichlet_bcs(self):
        bcs = []
        D = self._compressibility.get_displacement_space()
        for bc_spec in self.parameters["bc"]["dirichlet"]:
            val, marker, dof = bc_spec

            if type(marker) == int:
                args = [D.sub(dof), val, self.parameters["facet_function"], marker]
	    elif type(marker).__name__ == 'EdgeTypeBC':
                args = [D.sub(dof), val, marker, "pointwise"]
            else:
                args = [D.sub(dof), val, marker]

            bcs.append(DirichletBC(*args))

	
        return bcs 



