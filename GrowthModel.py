from dolfin import *

class GrowthLaw(object):

    def __init__(self, f0 = None, s0 = None, n0 = None, Theta1 = None, Theta2 = None, params = None):

	self.f0 = f0
	self.s0 = s0
	self.n0 = n0

	self.Theta1 = Theta1
	self.Theta2 = Theta2

        if params is None:
            params = self.default_parameters()

        for k,v in params.iteritems():
            setattr(self, k, v)



    def default_parameters(self):
        return {"beta1": 1.0,
		"beta2": 1.0,
		"scale": 1.0}

    def GrowthTensor(self):

	M1ij = self.f0[i]*self.f0[j]
    	M2ij = self.s0[i]*self.s0[j]
    	M3ij = self.n0[i]*self.n0[j]

    	Fg_t = self.Theta1*as_tensor(M1ij, (i,j)) + self.Theta2*(as_tensor(M2ij,(i,j)) + as_tensor(M3ij,(i,j)))

	return Fg_t


