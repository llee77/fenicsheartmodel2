#!/usr/bin/env python
# Copyright (C) 2016 Henrik Finsberg (Adapted by Lik Chuan Lee)
#
# This file is part of CAMPASS.
#
# CAMPASS is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CAMPASS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with CAMPASS. If not, see <http://www.gnu.org/licenses/>.


import sys
#sys.path.append('/home/likchuan/Dropbox/LVsolver_new')
sys.path.append('/home/lclee/Research/LVsolver_new')

from dolfin import *
from solver import Solver
from material import FungTransverse
from GrowthModel import GrowthLaw
import math
from addfiber_matid import *
from edgetypebc import *
import numpy as np



def setup_solver_parameters():
    
    solver_parameters = {"snes_solver":{}}

    solver_parameters["nonlinear_solver"] = NONLINSOLVER
    solver_parameters["snes_solver"]["method"] = SNES_SOLVER_METHOD
    solver_parameters["snes_solver"]["maximum_iterations"] = SNES_SOLVER_MAXITR
    solver_parameters["snes_solver"]["absolute_tolerance"] = SNES_SOLVER_ABSTOL 
    solver_parameters["snes_solver"]["linear_solver"] = SNES_SOLVER_LINSOLVER
    
    return solver_parameters
    
   

def setup_general_parameters():

    # Parameter for the compiler
    flags = ["-O3", "-ffast-math", "-march=native"]
    dolfin.parameters["form_compiler"]["quadrature_degree"] = 4
    dolfin.parameters["form_compiler"]["representation"] = "uflacs"
    dolfin.parameters["form_compiler"]["cpp_optimize"] = True
    dolfin.parameters["form_compiler"]["cpp_optimize_flags"] = " ".join(flags)
    
    dolfin.set_log_active(True)
    dolfin.set_log_level(ERROR)


def setup_application_parameters():

    params = Parameters("Application_parmeteres")    
    params.add("base_spring_k", 1.0)
    params.add("active_model", "active_strain")
    params.add("state_space", "P_2:P_1")
    params.add("compressibility", "incompressible", ["incompressible", 
                                                     "stabalized_incompressible", 
                                                     "penalty", "hu_washizu"])
    params.add("incompressibility_penalty", 10.0)
    
    material_parameters = Parameters("Material_parameters")
    material_parameters.add("mu", 10000)
    params.add(material_parameters)

def SimulatePassiveInflation(EDP, casename):

    setup_general_parameters()
    params = setup_application_parameters()

    mesh = Mesh(meshfilename)
    Quads = FunctionSpace(mesh, "Quadrature", 4)

    # Translate mesh
    ztop =  max(mesh.coordinates()[:,2])
    ztrans = Expression(("0.0", "0.0", str(-ztop)), degree = 1)
    
    if(dolfin.dolfin_version() != '1.6.0'):
    	ALE.move(mesh,ztrans)
    else:
    	mesh.move(ztrans)

    facetboundaries = MeshFunction('size_t', mesh, facetfilename)
    edgeboundaries = MeshFunction('size_t', mesh, edgefilename)
    endoring = pick_endoring_bc()(edgeboundaries, 1)

    press = Expression(("P"), P = 0, degree = 1)

    DG = FunctionSpace(mesh, "DG", 0)
    matid = Function(DG, matidfilename)
    matid.rename("matid", "matid")
    postinfarct_matid_gpt = matid.vector().array()[:]

    infarct_endo_angle = endo_angle
    infarct_epi_angle = epi_angle
    VDG = VectorFunctionSpace(mesh, "DG", 0)
    f0, s0, n0 = addfiber_matid(mesh, VDG, casename, endo_angle, epi_angle, infarct_endo_angle, infarct_epi_angle, casedir, postinfarct_matid_gpt)
 
    File("fiber.pvd") << f0

    # Facet Normal
    N = FacetNormal(mesh)

    material = FungTransverse(Identity(mesh.topology().dim()), f0, s0, n0)

    # Solver parameters
    solver_parameters = {"snes_solver":{}}
    solver_parameters["nonlinear_solver"] = "snes"
    solver_parameters["snes_solver"]["method"] = "newtonls"
    solver_parameters["snes_solver"]["maximum_iterations"] = 20
    solver_parameters["snes_solver"]["absolute_tolerance"] = 1e-7
    solver_parameters["snes_solver"]["report"] = True
    solver_parameters["snes_solver"]["linear_solver"] = "lu"

    # Create parameters for the solver
    params= {"mesh": mesh,
             "facet_function": facetboundaries,
             "facet_normal": N,
             "state_space": "P_2:P_1",
             "compressibility":{"type": "incompressible",
                                "lambda":0.0},
             "material": material,
             "bc":{"dirichlet": [[Constant("0"), endoring, 0], [Constant("0"), endoring, 1], [Constant("0"), endoring, 2], [Constant("0"), topid, 2]],
		   "neumann": [[press, endoid]]
                   },
             "endoid": endoid,
             "solve":solver_parameters}
           
    solver = Solver(params)

    nload = 15
    dispfile = File("disp_"+casename+".pvd")
    fdataPV = open("PV_"+casename+".txt", "w", 0)
    print >>fdataPV, press.P , solver.cavityvol()
    print "Pressure = ", press.P , "Volume = ", solver.cavityvol()
    for p in range(0,nload):

            press.P += EDP/float(nload)
            solver.solve()

    	    print "Pressure = ", press.P , "Volume = ", solver.cavityvol()
    	    u,p = solver.get_state().split() 
	    dispfile << u
	    print >>fdataPV, press.P , solver.cavityvol()

    # Save Homeostatic Lambda
    lbda = solver.fiber_stretch()
    lbda_gpt = project(lbda, Quads).vector().array()[:]
    File("lmbda_"+casename+".xml") << project(lbda, Quads)

    return lbda_gpt
    

def SimulateGrowth(lmbda, lmbda_homeo, beta1, beta2, casename):

    setup_general_parameters()
    params = setup_application_parameters()

    mesh = Mesh(meshfilename)
    Quads = FunctionSpace(mesh, "Quadrature", 4)

    # Translate mesh
    ztop =  max(mesh.coordinates()[:,2])
    ztrans = Expression(("0.0", "0.0", str(-ztop)), degree = 1)
    
    if(dolfin.dolfin_version() != '1.6.0'):
    	ALE.move(mesh,ztrans)
    else:
    	mesh.move(ztrans)

    facetboundaries = MeshFunction('size_t', mesh, facetfilename)
    edgeboundaries = MeshFunction('size_t', mesh, edgefilename)
    endoring = pick_endoring_bc()(edgeboundaries, 1)

    DG = FunctionSpace(mesh, "DG", 0)
    matid = Function(DG, matidfilename)
    matid.rename("matid", "matid")
    postinfarct_matid_gpt = matid.vector().array()[:]

    infarct_endo_angle = endo_angle
    infarct_epi_angle = epi_angle
    VDG = VectorFunctionSpace(mesh, "DG", 0)
    f0, s0, n0 = addfiber_matid(mesh, VDG, casename, endo_angle, epi_angle, infarct_endo_angle, infarct_epi_angle, casedir, postinfarct_matid_gpt)

    # Facet Normal
    N = FacetNormal(mesh)

    # Set up material model
    dTheta1 = Function(Quads)
    dTheta2 = Function(Quads)
    dTheta1.vector()[:] = beta1*(lmbda - lmbda_homeo)
    dTheta2.vector()[:] = beta2*(lmbda - lmbda_homeo)

    scale = Expression(("value"), value = 0.2, degree = 1)

    Theta1 = Function(Quads)
    Theta2 = Function(Quads)
    Theta1 = Constant("1.0") + scale*dTheta1*scale 
    Theta2 = Constant("1.0") + scale*dTheta2*scale

    growlaw = GrowthLaw(f0, s0, n0, Theta1, Theta2)
    material = FungTransverse(growlaw.GrowthTensor(), f0, s0, n0)

    # Solver parameters
    solver_parameters = {"snes_solver":{}}
    solver_parameters["nonlinear_solver"] = "snes"
    solver_parameters["snes_solver"]["method"] = "newtonls"
    solver_parameters["snes_solver"]["maximum_iterations"] = 20
    solver_parameters["snes_solver"]["absolute_tolerance"] = 1e-7
    solver_parameters["snes_solver"]["report"] = True
    solver_parameters["snes_solver"]["linear_solver"] = "lu"

    # Create parameters for the solver
    params= {"mesh": mesh,
             "facet_function": facetboundaries,
             "facet_normal": N,
             "state_space": "P_2:P_1",
             "compressibility":{"type": "incompressible",
                                "lambda":0.0},
             "material": material,
             "bc":{"dirichlet": [[Constant("0"), endoring, 0], [Constant("0"), endoring, 1], [Constant("0"), endoring, 2], [Constant("0"), topid, 2]]
                   },
             "endoid": endoid,
             "solve":solver_parameters}
           
    solver = Solver(params)

    nload = 5
    dispfile = File("disp_"+casename+".pvd")
    fdataPV = open("PV_"+casename+".txt", "w", 0)
    print >>fdataPV, 0.0 , solver.cavityvol()
    print "Pressure = ", 0.0 , "Volume = ", solver.cavityvol()
    for p in range(0,nload):

	    scale.value = float(p+1)/float(nload)
            solver.solve()

    	    print "Loadstep = ", p , "Volume = ", solver.cavityvol(), "scale = ", scale.value
    	    u,p = solver.get_state().split() 
	    dispfile << u
	    print >>fdataPV, 0.0 , solver.cavityvol()

    # Save Lambda
    #lbda = solver.fiber_stretch()
    #lbda_gpt = project(lbda, Quads).vector().array()[:]
    #File("lmbda_"+casename+".xml") << project(lbda, Quads)

    # Save mesh
    ALE.move(mesh,u)
    File("mesh_"+casename+".xml") << mesh
    interactive()

if __name__ == "__main__":

    beta1 = 1.0
    beta2 = 0.0

    os.system("rm *.pvd")
    os.system("rm *.vtu")
    os.system("rm *.xml")
    os.system("rm *.txt")
    
    casedir = "../mesh/"
    casename = '../mesh/pmr120_baselinetri'
    meshfilename = casename + ".xml"
    facetfilename = casename + "_facet_region.xml"
    edgefilename = casename + "_edge_region.xml"
    subdomainfilename = casename + "_physical_region.xml"
    matidfilename = casename + "_matid.xml"
    
    topid = 4
    endoid = 3
    epiid = 2
    endo_angle = 83
    epi_angle = -37


    # Calculate homeostatic 
    casename = "homeo"
    lmbda_homeo = SimulatePassiveInflation(580, casename)
    lmbda_homeo = Function(FunctionSpace(Mesh(meshfilename), "Quadrature", 4), "lmbda_homeo.xml").vector()[:]

    # Growth 
    for gstep in range(0, 5):
    	casename = "step" + str(gstep)

    	lmbda = SimulatePassiveInflation(1080, casename)
	xmlfilename = "lmbda_" + casename + ".xml"
    	lmbda = Function(FunctionSpace(Mesh(meshfilename), "Quadrature", 4), xmlfilename).vector()[:]

	growthcasename = casename+"_growth"
    	SimulateGrowth(lmbda, lmbda_homeo, beta1, beta2, growthcasename)
	meshfilename = "mesh_"+growthcasename+".xml"
