#!/usr/bin/env python
# Copyright (C) 2016 Henrik Finsberg (Adapted by Lik Chuan Lee)
#
# This file is part of CAMPASS.
#
# CAMPASS is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CAMPASS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with CAMPASS. If not, see <http://www.gnu.org/licenses/>.


import sys
#sys.path.append('/home/likchuan/Dropbox/LVsolver_new')
sys.path.append('/home/lclee/Research/LVsolver_new')

from dolfin import *
from solver import Solver
from material import FungTransverse
from GrowthModel import GrowthLaw
from activematerial import SimpleActive
from updateArtsVariable import updateArtsVariable
import math
from addfiber_matid import *
from edgetypebc import *
import numpy as np
from matplotlib import pylab as plt



def setup_solver_parameters():
    
    solver_parameters = {"snes_solver":{}}

    solver_parameters["nonlinear_solver"] = NONLINSOLVER
    solver_parameters["snes_solver"]["method"] = SNES_SOLVER_METHOD
    solver_parameters["snes_solver"]["maximum_iterations"] = SNES_SOLVER_MAXITR
    solver_parameters["snes_solver"]["absolute_tolerance"] = SNES_SOLVER_ABSTOL 
    solver_parameters["snes_solver"]["linear_solver"] = SNES_SOLVER_LINSOLVER
    
    return solver_parameters
    
   

def setup_general_parameters():

    # Parameter for the compiler
    flags = ["-O3", "-ffast-math", "-march=native"]
    dolfin.parameters["form_compiler"]["quadrature_degree"] = 4
    dolfin.parameters["form_compiler"]["representation"] = "uflacs"
    dolfin.parameters["form_compiler"]["cpp_optimize"] = True
    dolfin.parameters["form_compiler"]["cpp_optimize_flags"] = " ".join(flags)
    
    dolfin.set_log_active(True)
    dolfin.set_log_level(ERROR)


def setup_application_parameters():

    params = Parameters("Application_parmeteres")    
    params.add("base_spring_k", 1.0)
    params.add("active_model", "active_strain")
    params.add("state_space", "P_2:P_1")
    params.add("compressibility", "incompressible", ["incompressible", 
                                                     "stabalized_incompressible", 
                                                     "penalty", "hu_washizu"])
    params.add("incompressibility_penalty", 10.0)
    
    material_parameters = Parameters("Material_parameters")
    material_parameters.add("mu", 10000)
    params.add(material_parameters)

def SimulateCardiacCycle(EDP, casename):

    setup_general_parameters()
    params = setup_application_parameters()

    mesh = Mesh(meshfilename)

    Quadelem = FiniteElement("Quadrature",  mesh.ufl_cell(), 4, quad_scheme="default")
    Quads = FunctionSpace(mesh, Quadelem)

    Tact = Function(Quads)
    Lart = Function(Quads)
    Cart = Function(Quads)

    # Translate mesh
    ztop =  max(mesh.coordinates()[:,2])
    ztrans = Expression(("0.0", "0.0", str(-ztop)), degree = 1)
    
    if(dolfin.dolfin_version() != '1.6.0'):
    	ALE.move(mesh,ztrans)
    else:
    	mesh.move(ztrans)

    facetboundaries = MeshFunction('size_t', mesh, facetfilename)
    edgeboundaries = MeshFunction('size_t', mesh, edgefilename)
    endoring = pick_endoring_bc()(edgeboundaries, 1)

    press = Expression(("P"), P = 0, degree = 1)

    DG = FunctionSpace(mesh, "DG", 0)
    matid = Function(DG, matidfilename)
    matid.rename("matid", "matid")
    postinfarct_matid_gpt = matid.vector().array()[:]

    infarct_endo_angle = endo_angle
    infarct_epi_angle = epi_angle
    VDG = VectorFunctionSpace(mesh, "DG", 0)
    f0, s0, n0 = addfiber_matid(mesh, VDG, casename, endo_angle, epi_angle, infarct_endo_angle, infarct_epi_angle, casedir, postinfarct_matid_gpt)
 
    File("fiber.pvd") << f0

    # Facet Normal
    N = FacetNormal(mesh)

    material = FungTransverse(Identity(mesh.topology().dim()), f0, s0, n0)
    activemat = SimpleActive(Tact, f0, s0, n0)

    # Cavity Volume Expression
    V0 = Expression("vol", vol=47, degree = 1)

    # Solver parameters
    solver_parameters = {"snes_solver":{}}
    solver_parameters["nonlinear_solver"] = "snes"
    solver_parameters["snes_solver"]["method"] = "newtonls"
    solver_parameters["snes_solver"]["maximum_iterations"] = 20
    solver_parameters["snes_solver"]["absolute_tolerance"] = 1e-7
    solver_parameters["snes_solver"]["report"] = True
    solver_parameters["snes_solver"]["linear_solver"] = "mumps"
    #solver_parameters['snes_solver']['line_search'] = 'bt'

    # Create parameters for the solver
    params= {"mesh": mesh,
             "facet_function": facetboundaries,
             "facet_normal": N,
             "state_space": "P_2:P_1:Real",
             "compressibility":{"type": "incompressible",
                                "lambda":0.0},
             "material": material,
             "activematerial": activemat,
             "bc":{"dirichlet": [[Constant("0"), endoring, 0], [Constant("0"), endoring, 1], [Constant("0"), endoring, 2], [Constant("0"), topid, 2]]},#;
                   #"neumann": [[press,endoid]]},
             "endoid": endoid,
             "solve":solver_parameters,
             #"volume_control": False,
             "volume_control": True,
	     "constrained_vol": V0}
           
    solver = Solver(params)

    nload = 5
    dispfile = File("disp_"+casename+".pvd")
    fdataPV = open("PV_"+casename+".txt", "w", 0)
    print >>fdataPV, press.P , solver.cavityvol()
    print "Pressure = ", press.P , "Volume = ", solver.cavityvol()

    V0.vol = solver.cavityvol()
    Initvol = V0.vol
    t_a = 0.0
    dt = 1.0
    EDV = 60

    # Initialize variables
    Lart.vector()[:] = (1.85 - 0.04)*np.ones(len(Lart.vector().array()[:])) 

    # Loading phase
    for p in range(0,nload):

    	    V0.vol += (EDV - Initvol)/float(nload)

            solver.solve()

	
	    if(len(solver.get_state().split()) == 2):
    	    	u,p = solver.get_state().split() 
    		print "Pressure = ", press.P , "Volume = ", solver.cavityvol()
	        print >>fdataPV, press.P , solver.cavityvol()

	    else:
    	    	u,p,Pendo = solver.get_state().split() 
    	        print "Pressure = ", solver.cavitypressure() , "Volume = ", solver.cavityvol()
	        print >>fdataPV, solver.cavitypressure() , solver.cavityvol()

	    dispfile << u

    # Closed loop cycle
    BCL = 800.0
    tstep = 0
    Cao = 10.0/1000.0;
    Cven = 400.0/1000.0;
    Vart0 = 510;#500;
    Vven0 = 2800;#3200.0;
    Rao = 5*1000.0 ;
    Rven = 2.0*1000.0;
    Rper = 10*1000.0;
    V_ven = 3660 
    V_art = 640
    
    cycle = 0.0
    outputgpt = 2
    t = 0
    tstep = 0

    LVcav_array = [solver.cavityvol()]
    Pcav_array = [solver.cavitypressure()*0.0075]

    fig = plt.figure()
    ax = fig.add_subplot(111)
    li, = ax.plot(LVcav_array, Pcav_array)
    fig.canvas.draw()
    plt.show(block=False)


    while(cycle < 3):#Tend):

       	dt = 1.0
        p_cav = solver.cavitypressure() 
        V_cav = solver.cavityvol()

    	print "time = ", t
    	tstep = tstep + dt
    
        cycle = math.floor(tstep/BCL)
    	t = tstep - cycle*BCL
    
        lbda = project(solver.fiber_stretch(), Quads)
	Tact, Lart, Cart = updateArtsVariable(150e5, Tact, Lart, Cart, dt, lbda, t)
    		
    	print "Cycle number = ", cycle, " cell time = ", t, " tstep = ", tstep, " dt = ", dt
	print max(Tact.vector()[:]), min(Tact.vector()[:])
       
    	print >>fdataPV, tstep, p_cav , V_cav
    
    	Part = 1.0/Cao*(V_art - Vart0);
        Pven = 1.0/Cven*(V_ven - Vven0);
        PLV = p_cav;
    
    	print "P_ven = ",Pven;
        print "P_LV = ", PLV;
        print "P_art = ", Part;
    
        if(PLV <= Part):
             Qao = 0.0;
        else:
             Qao = 1.0/Rao*(PLV - Part);
        
    
        if(PLV >= Pven):
            Qmv = 0.0;
        else: 
            Qmv = 1.0/Rven*(Pven - PLV);
        
    
        Qper = 1.0/Rper*(Part - Pven);
    
        print "Q_mv = ", Qmv ;
        print "Q_ao = ", Qao ;
        print "Q_per = ", Qper ;
    
    	V_cav_prev = V_cav
    	V_art_prev = V_art
    	V_ven_prev = V_ven
    	p_cav_prev = p_cav
    
        V_cav = V_cav + dt*(Qmv - Qao);
        V_art = V_art + dt*(Qao - Qper);
        V_ven = V_ven + dt*(Qper - Qmv);
    
        print "V_ven = ", V_ven;
        print "V_LV = ", V_cav;
        print "V_art = ", V_art;
    
	V0.vol = V_cav

	LVcav_array.append(V_cav)
	Pcav_array.append(p_cav*0.0075)
	li.set_xdata(LVcav_array)
	li.set_ydata(Pcav_array)
	plt.xlim([min(LVcav_array)-10,max(LVcav_array)+10])
	plt.ylim([min(Pcav_array)-10,max(Pcav_array)+10])
        fig.canvas.draw()


        solver.solve()
	dispfile << u


if __name__ == "__main__":

    beta1 = 1.0
    beta2 = 0.0

    os.system("rm *.pvd")
    os.system("rm *.vtu")
    os.system("rm *.xml")
    os.system("rm *.txt")
    
    casedir = "../mesh/"
    casename = '../mesh/pmr120_baselinetri'
    meshfilename = casename + ".xml"
    facetfilename = casename + "_facet_region.xml"
    edgefilename = casename + "_edge_region.xml"
    subdomainfilename = casename + "_physical_region.xml"
    matidfilename = casename + "_matid.xml"
    
    topid = 3
    endoid = 2
    epiid = 1

    endo_angle = 60
    epi_angle = -60

    # Calculate homeostatic 
    casename = "homeo"
    lmbda_homeo = SimulateCardiacCycle(580, casename)


