import numpy as np

def updateArtsVariable(Tmax, Tact, Lart, Cart, dt, lbdaa, t_a):


	L = Lart.vector().array()[:]
	C = Cart.vector().array()[:]
	lbda = lbdaa.vector().array()[:]

	Crest = 0.04
	Lsc0 = 1.51
	Lsref = 1.85
	Lseiso = 0.04
	vmax = 7.0e-3
	tauD = 32.0
	tauR = 48.0
	tauSC = 425.0

	#lbda = 1.2*np.ones(len(lbda))
	Ls = lbda*Lsref	

	#Lnew =  L + dt*(Ls/Lseiso - 1.0)*vmax
	#Lnew = Lnew /(1.0 + dt*vmax/Lseiso)

	Lnew =  L + dt*((Ls - L)/Lseiso - 1.0)*vmax
	#Lnew = (Ls < Lnew_t)*Ls + (Ls >= Lnew_t)*Lnew_t

	
	TLsc = tauSC*(0.29 + 0.3*(Lnew))
	CLsc = np.tanh(4.0*(Lnew - Lsc0)**2.0)
	coef = (1.0 + np.exp((TLsc - t_a)/tauD))

	xp = min(8.0, max(0.0, t_a/tauR))
	Frise = 0.02*(xp**3.0)*(8.0 - xp)**2.0*np.exp(-xp);
	
	Cnew = C + dt/tauR*CLsc*Frise + dt/tauD*(Crest/coef)
	Cnew = Cnew/(1.0 + dt/(tauD*coef))

	print "Lnew = ", Lnew[1], " Ls = ", Ls[1], " lmbda = ", lbda[1], "xp = ", xp, "Frise = ", Frise
	print "Cnew = ", Cnew[1], " Lsc0 = ", Lsc0

	Tact_t = Tmax * Frise * Cnew #* (Lnew - Lsc0) * (Ls - Lnew) / Lseiso
	#Tact_t = Tmax * Cnew * (Lnew - Lsc0) #* (Ls - Lnew) / Lseiso * (Lnew > Lsc0)
	Tact.vector()[:] = Tact_t * (Lnew > Lsc0) #* (Ls > Lnew)  + 0.0

	Lart.vector()[:] = L 
	Cart.vector()[:] = C 


	return Tact, Lart,  Cart 
	
	
	






